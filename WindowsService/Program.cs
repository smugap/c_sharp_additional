﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace WindowsService
{
    class Program
    {
        static void Main(string[] args)
        {
            //using Topshelf by nuget
            //po debugu jak chcesz uruchomić to wchodzić w konsoli do pliku exe i install

            HostFactory.Run(z =>
            {
                z.Service<ServiceCore>(s =>
                {
                    s.ConstructUsing(c => new ServiceCore());
                    s.WhenStarted(c => c.Start());
                    s.WhenStopped(c => c.Stop());
                });
                z.RunAsLocalSystem();
                z.StartManually();
                z.SetDescription("Test service by Piotr Smuga");
            });

            //do servisów można użyć dziennika zdarzeń winsows
            //str 542 ksiązka

           // Console.ReadKey();
        }
    }
}
