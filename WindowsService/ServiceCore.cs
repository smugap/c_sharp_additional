using System;
using System.Globalization;
using System.IO;
using System.Timers;

namespace WindowsService
{
    public class ServiceCore
    {
        private Timer timer;

        public ServiceCore()
        {
            timer = new Timer(10000);
            timer.Start();
            timer.Elapsed += (sender, args) =>
            {
                string message = $"{DateTime.Now.ToString(CultureInfo.CurrentCulture)} - I'm working! {Environment.NewLine}";
                File.AppendAllText("D:/test.txt", message);
                Console.WriteLine(message);
            };
        }

        public void Start()
        {
            Console.WriteLine("Service started");
        }

        public void Stop()
        {
            timer.Stop();
            Console.WriteLine("Service stopped");
        }
    }
}