﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using Newtonsoft.Json;


namespace JsonExample
{
    class Program
    {
        static void Main(string[] args)
        {
            KursGankiewicz.User user = new KursGankiewicz.User("email@gmail.com","passwordPrivate");

            string json = JsonConvert.SerializeObject(user);
            Console.WriteLine(json);

            KursGankiewicz.User userCopy = JsonConvert.DeserializeObject<KursGankiewicz.User>(json);
            
            Console.ReadKey();
        }
    }
}
