﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using Testing;
using Moq;
using FluentAssertions;
namespace TestingTests
{
    [TestClass]
    public class UserTest
    {
        //Testowanie moze być Nlog lub in default microsoft testing framework (MSTEst2)
        //można zaisntalować również Moq oraz FluentAssertions

        //stworzyć nowy projekt Unit Test Project
        //dodac referencje do projektu który chcemy testowac

        //how to with NUnit
        //https://www.visualstudio.com/en-us/docs/test/developer-testing/getting-started/getting-started-with-developer-testing#frameworks

           
    

        [TestMethod]
        public void changing_email_should_successed()
        {
            //
            var destinationEmail = "email@gmail.com";
            User u = new User("email1@gmail.com","secret");

            u.SetEmail(destinationEmail);

            //default
            Assert.AreEqual(u.Email,destinationEmail);

            //fluent
            //u.Email.ShouldBeEquivalentTo(destinationEmail);
        }

        [TestMethod]
        public void providing_empty_email_should_fail()
        {
            User u = new User("email1@gmail.com", "secret");

            var exception = Assert.ThrowsException<Exception>(() => u.SetEmail(string.Empty));

            
            Assert.IsNotNull(exception);
            Assert.IsTrue(exception.Message.StartsWith("Email cannot be empty."));


            //fluent
            //Action act = () => u.SetEmail(string.Empty);
            //act.ShouldThrow<Exception>();
        }

        [TestMethod]
        public void providing_empty_password_should_fail_with_message_strating_with_wrong()
        {
            User u = new User("email1@gmail.com", "secret");
            
            //u.SetPassword(String.Empty);

            Action act = () => u.SetPassword(String.Empty);
            act.ShouldThrow<Exception>().And.Message.Should().StartWith("Wrong");
        }
    }
}
