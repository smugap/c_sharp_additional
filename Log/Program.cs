﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Log
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            //Install-Package NLog.Config
            //i pózniej skonfigurować NLog.config
            logger.Info("Create main window");

            test();
            Console.ReadKey();
        }

        private static void test()
        {
            logger.Warn("testowe ostrzeżenie");
        }
    }
}
