﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SitoEratostenesa
{
    class Program
    {
        private static ulong N = 1000000000;
        static void Main(string[] args)
        {
            Stopwatch zegar = new Stopwatch();

            zegar.Start();
            SitoEratostenesa();
            zegar.Stop();
            Console.WriteLine($"Eratostenes 1: {zegar.Elapsed}");

            zegar.Restart();
            SitoEratostenesa2();
            zegar.Stop();
            Console.WriteLine($"Eratostenes 2: {zegar.Elapsed}");

            zegar.Restart();
            SitoEratostenesa3();
            zegar.Stop();
            Console.WriteLine($"Eratostenes 3: {zegar.Elapsed}");


            //do 1000 2 sito najlepsze
            //do 10 000 tak samo
            //do 100 000 tak samo
            //do 1 000 000 3 sito najlepsze
            //do 10 000 000 3 sito najlepsze
            //do 100 000 000 3 sito najlepsze
            //do 1 000 000 000 2 sito najlepsze


            Console.ReadKey();
        }

        static void SitoEratostenesa()
        {
            bool[] S = new bool[N+1];
            for (ulong i = 0; i <= N; i++)
            {
                S[i] = true;
            }
            ulong g = (ulong) Math.Sqrt(N);
            ulong w;
            for (ulong i = 2; i <= g; i++)
            {
                if (S[i])
                {
                    w = i * i;
                    while (w<=N)
                    {
                        S[w] = false;
                        w += i;
                    }
                }
            }
            //for (ulong i = 2; i <= N; i++)
            //{
            //    if (S[i])
            //    {
            //        Console.Write($"{i} ");
            //    }
            //}
            //Console.WriteLine();
        }

        static void SitoEratostenesa2()
        {
            ulong m = N >> 1;
            bool[] S = new bool[m + 1];
            for (ulong i = 1; i < m; i++)
            {
               S[i] = true;
            }
            ulong p = 3, q = 4, ii = 1;
            ulong k;
            do
            {
                if (S[ii])
                {
                    k = q;
                    while (k<m)
                    {
                        S[k] = false;
                        k += p;
                    }
                }
                ii++;
                p += 2;
                q += (p << 1) - 2;
            } while (q<m);
            //Console.Write("2 ");
            //for (ulong i = 1; i < m; i++)
            //{
            //    if (S[i])
            //    {
            //        Console.Write($"{(i<<1)+1} ");
            //    }
            //}
            //Console.WriteLine();
        }

        static void SitoEratostenesa3()
        {
            ulong m = N / 3;
            bool[] S = new bool[m + 1];
            ulong c = 0, k = 1, t = 2;
            for (ulong i = 1; i <= m; i++)
            {
                S[i] = true;
            }
            ulong q = (ulong)Math.Sqrt(N) / 3;
            ulong j,ij;


            for (ulong i = 1; i <= q; i++)
            {
                k = 3 - k;
                c += (k << 2) * i;
                j = c;
                ij = (i << 1) * (3 - k) + 1;
                t += k << 2;
                while (j <= m)
                {
                    S[j] = false;
                    j += ij;
                    ij = t - ij;
                }
            }
            //Console.Write("2 3 ");
            //for (ulong i = 1; i <= m; i++)
            //    if (S[i])
            //    {
            //        if ((i & 1) != 0)
            //            Console.Write(3*i+2);
            //        else
            //            Console.Write(3 * i + 1);
            //        Console.Write(" ");
            //    }
            //Console.WriteLine();
        }
    }
}
