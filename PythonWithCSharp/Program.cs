﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPython.Hosting;

namespace PythonWithCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //you have to install IronPython
            //dodac plik tekstowy i zmienic na nazwa.py
            //po zaznaczeniu pliku zmeinić własciwosc copy to output directory na always copy

            //Console.WriteLine("What would you like to print from python?");
            //var input = Console.ReadLine();

            //var py = Python.CreateEngine();
            //try
            //{
            //    py.Execute("print('From Python: " + input + "')");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(
            //        "Oops! We couldn't print your message because of an exception: " + ex.Message);
            //}


            //Console.WriteLine("Press enter to execute the python script!");
            //Console.ReadLine();

            //var py = Python.CreateEngine();
            //try
            //{
            //    py.ExecuteFile("sript.py");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(
            //        "Oops! We couldn't execute the script because of an exception: " + ex.Message);
            //}


            var ipy = Python.CreateRuntime();
            dynamic test = ipy.UseFile("test.py");
            test.Simple();

            Console.ReadKey();
        }
    }
}
