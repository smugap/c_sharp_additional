﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace KursGankiewicz
{
    class Asynchronous
    {
        public async Task Test()
        {
            var content = await GetContentAsync();
            Console.WriteLine(content);
        }

        public async Task<string> GetContentAsync()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync("https://jsonplaceholder.typicode.com/photos");
            var content = await response.Content.ReadAsStringAsync();

            return content;


            //1 await doSth czyli wsylis zapytanie GetAsync..
            //do sth different
            // await get value from 1
        }
    }
}
