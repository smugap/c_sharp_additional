﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Framework;

namespace KursGankiewicz
{
    public class User
    {
        [Required]
        public string Email{get; set;}

        [UserPasswordAtrribute]
        public string Password { get; set; }        
        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }


}
