﻿using System;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace KursGankiewicz
{
    class Program
    {
        static void Main(string[] args)
        {
            //Epizod6();

            //var asynchron = new Asynchronous();
            //asynchron.Test().Wait();//daje wait bo aplikacja konsolowa 


            //szybsze bo na kilku wątkach, jeśli coś dużo robimy
            var numbers = Enumerable.Range(0, 1000);
            Stopwatch s = Stopwatch.StartNew();
            Parallel.ForEach(numbers, number =>
            {
                Console.WriteLine($"Number {number} on thread {Thread.CurrentThread.ManagedThreadId}.");
                Thread.Sleep(100);
            });
            s.Stop();
            
            Stopwatch s2= Stopwatch.StartNew();
            s2.Start();
            foreach (var number in numbers)
            {
                Console.WriteLine($"Number {number} on thread {Thread.CurrentThread.ManagedThreadId}.");
                Thread.Sleep(100);
            }

            s2.Stop();
            Console.WriteLine($"Paraller.Foreach: {s.Elapsed.Seconds} seconds");
            Console.WriteLine($"Foreach: {s2.Elapsed.Seconds} seconds");


            Console.ReadKey();
        }
        static void Epizod5()
        {
            //tworzy tliste z liczbami z przedziału od 1 do 100
            var numbers = Enumerable.Range(1, 100).ToList(); 
            //gdy korzystamy z IQueryable to po wywołaniu lepiej stosowac np ToList()
        }

        private static void Epizod6()
        {
            //reflections
            var user = new User("email@gmail.com", "1234");
            var type = user.GetType(); // w core jeszcze trzeba dodac .GetTypeInfo();
            Console.WriteLine($"{type.Name} {type.Namespace} {type.FullName}");

            var methods = type.GetMethods();
            foreach(var method in methods)
            {
                Console.WriteLine($"{method.Name}");
            }

            var setEmail = type.GetMethod("set_Email");
            setEmail.Invoke(user, new[] { "email2@gmail.com" });//jak nie ma paramterow funkcjia to przesyłą się jako drugi paremetr null, inaczej tablice paramterów
            Console.WriteLine($"Email: {user.Email}");

            var email = type.GetProperty("Email")?.GetValue(user);//uzyskanei dostepu do emaial przy okazji refelksji
            Console.WriteLine($"Email: {email}");


            var epizodTypes = Assembly.GetEntryAssembly().GetTypes().Where(x => x.Name.Contains("User"));//szuka w cąłym namespace klas zawieracjacym słowo User 
            foreach (var epizodType in epizodTypes)
            {
                Console.WriteLine($"{epizodType} ");
            }

            var user2 = Activator.CreateInstance(typeof(User), "email3@gmail.com", "password");//lepiej tak nie tworzyć nowych obiektow lepiej wzorce projektowe użyc
            Console.WriteLine($"user2: {user.Email} ");


            //typy dynamcizne, czesto stosowane w połączeniech z jakimiś zewnetrznymi danymi, w których wiemy ajkei mają klasy 
            dynamic anything = new ExpandoObject();
            anything.id = 1;
            anything.name = "me";
            Console.WriteLine($"{anything.id} {anything.name}");
            foreach (var item in anything)
            {
                Console.WriteLine($"{item.Key} {item.Value}");
            }

            //atrybuty
            var user3 = new User("dfdd@dfd.com","secre");
            var passwordAttribute = (UserPasswordAtrribute) user3.GetType().GetProperty("Password")
                .GetCustomAttribute(typeof(UserPasswordAtrribute));
            var isPasswordValid = user3.Password.Length == passwordAttribute.Lenght;
            Console.WriteLine($"Is valid: {isPasswordValid}");
        }

    }

}
