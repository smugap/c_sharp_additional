﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    public class User
    {
        public  string Email { get; private set; }
        public  string Password { get; private set; }
        public User(string email, string haslo)
        {
            Email = email;
            Password = haslo;
        }

        public void SetEmail(string email)
        {
            if (email == Email)
                return;
            if (string.IsNullOrEmpty(email))
                throw  new Exception("Email cannot be empty.");

            Email = email;

        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new Exception("Wrong password");
            if(Password.Equals(password))
                return;
            Password = password;
        }
    }
}
